import sys, os, hashlib, random, string

from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES
from pathlib import Path

def start():

	print("Utilisateur: ")
	user=input()
	print("Entrez le mot de passe")
	mdp1 = input()
	mdp1=mdp1.encode()
	cipherKey1 = AES.new(mdp1)
	print("Insérez la clé")	
	if user=="Responsable":
		key1F = open("usb1/key1", "rb")
		idResp1F=open("ramdisk/Resp1")
		idResp1=idResp1F.readline()
		idResp1F.close()
		key1=key1F.readline()
		tmp=key1F.readline()
		while(tmp!=""):
			key1=key1+tmp
			tmp=key1F.readline()
			break
		key1=cipherKey1.decrypt(key1)
		key1F.close()
		idg = key1[-4:].decode()
		if idg== idResp1:
			print("Authentification réussie: vous êtes identifié en tant que Responsable 1")
		else:
			print("Echec de l'authentification")
	elif user=="Représentant":
		key1F = open("usbRep1/key1", "rb")
		idRep1F=open("ramdisk/Rep1")
		idRep1=idRep1F.readline()
		idRep1F.close()
		key1=key1F.readline()
		tmp=key1F.readline()
		while(tmp!=""):
			key1=key1+tmp
			tmp=key1F.readline()
			break
		key1=cipherKey1.decrypt(key1)
		key1F.close()
		idg = key1[-4:].decode()
		if idg==idRep1:
			print("Authentification réussie: vous êtes identifié en tant que Représentant 1")
		else:
			print("Echec de l'authentification")
	else: 
		print("Utilisateur inconnu")
	

	print("Utilisateur: ")
	user=input()
	print("Entrez le mot de passe")
	mdp2 = input()
	mdp2=mdp2.encode()
	cipherKey2 = AES.new(mdp2)
	print("Insérez la clé")
	if user=="Responsable":
		key2F = open("usb2/key2", "rb")
		idResp2F=open("ramdisk/Resp2")
		idResp2=idResp2F.readline()
		idResp2F.close()
		key2=key2F.readline()
		tmp=key2F.readline()
		while(tmp!=""):
			key2=key2+tmp
			tmp=key2F.readline()
			break
		key2 = cipherKey2.decrypt(key2)
		key2F.close()
		idg= key2[-4:].decode()
		if idg== idResp2:
			print("Authentification réussie: vous êtes identifié en tant que Responsable 2")
		else:
			print("Echec de l'authentification")
	elif user=="Représentant":
		key2F = open("usbRep2/key2", "rb")
		idRep2F=open("ramdisk/Rep2")
		idRep2=idRep2F.readline()
		idRep2F.close()
		key2=key2F.readline()
		tmp=key2F.readline()
		while(tmp!=""):
			key2=key2+tmp
			tmp=key2F.readline()
			break
		key2 = cipherKey2.decrypt(key2)
		key2F.close()
		idg = key2[-4:].decode()
		if idg==idRep2:
			print("Authentification réussie: vous êtes identifié en tant que Représentant 2")
		else:
			print("Echec de l'authentification")
	else: 
		print("Utilisateur inconnu")
	
	key1=key1[:28]
	cipherKey1 = AES.new(mdp1)
	key1=cipherKey1.decrypt(key1+"0000".encode())
	#Enlever les 4 derniers bytes de chaque clé pour permettre l'identification du rôle
	mainKeyCrypted = open("ramdisk/mainKey", "rb")

	cipherKey2 = AES.new(mdp2)
	key2=cipherKey1.decrypt(key2)

	cipherMainKey2 = AES.new(key2)
	mainKeyTmp = cipherMainKey2.decrypt(mainKeyCrypted.readline())
	cipherMainKey1 = AES.new(key1)
	mainKey= cipherMainKey1.decrypt(mainKeyTmp)

	#decrypt("dd/cards.txt", mainKey)

def addCard(numero, nom):
	#utiliser la clé maitre pour crypter le numéro de carte puis l'ajouter avec le nom
	file = open("dd/cards.txt","a")
	file.write(nom+" "+numero+"\n")	

def delCard(numero, nom):
	# Effacer la ligne correspondant au nom, au numéro ou aux deux
	file = open("dd/cards.txt","r")
	ls = file.readlines()

	tmp = open("dd/cards1.txt","w")
	for l in ls:
		if (l!= nom+" "+numero+"\n"):
			tmp.write(l)

	file = open("dd/cards.txt","w")
	tmp = open("dd/cards1.txt","r")
	for l in tmp.readlines():
		file.write(l)

	os.remove("dd/cards1.txt")

def findCard(nom):
	#Rechercher par nom les cartes correspondantes
	file = open("dd/cards.txt","r")
	ls = file.readlines()
	ind=[]
	for i in ls:
		h = i.split()
		if h[0]==nom:
			ind.append(h[1])

	for c in ind:
		print(c)

def init():
	# Clés des responsables à encoder
	key1 = ''.join(random.choice(string.ascii_lowercase) for i in range(28))
	key2 = ''.join(random.choice(string.ascii_lowercase) for i in range(28))

	key1 =key1.encode()
	key2 =key2.encode()

	passResp1 = "bh59mkojnjbhjbrzg98/pkniuhetdszw"
	passResp2 = "*poklnknnjjn/85jidbsincisnzbsqxz"
	
	#Mots de passe connus par les représentants
	passRep1 = "mnuhfbkbgrkjbdkbehkbb59464ddsfza"
	passRep2 = "njfbehbqkhb51471rnjnj5/+64fdsmvd"

	#Passage de type str à bytes pour pouvoir crypter
	passResp1 = passResp1.encode()
	passResp2 = passResp2.encode()

	passRep1 = passRep1.encode()
	passRep2 = passRep2.encode()
	encrypt("test.txt", passRep1)
	#Création d'une instance d'AES permettant l'encodage à partir des mots de passe de responsables
	cipherResp1= AES.new(passResp1)
	cipherResp2 = AES.new(passResp2)

	#Création d'une instance d'AES permettant l'encodage à partir des mots de passe de représentants
	cipherRep1 = AES.new(passRep1)
	cipherRep2 = AES.new(passRep2)	

	#Création de l'identifiant des responsables
	endResp1 = ''.join(random.choice(string.ascii_lowercase) for i in range(4))
	endResp2 = ''.join(random.choice(string.ascii_lowercase) for i in range(4))

	idResp1 = endResp1.encode()
	idResp2 = endResp2.encode()

	file = open("ramdisk/Resp1", "wb")
	file.write(idResp1)

	file = open("ramdisk/Resp2", "wb")
	file.write(idResp2)

	#Ajout de l'identifiant de responsable à la clé
	keyResp1 = key1+idResp1
	keyResp2 = key2+idResp2

	#Création de l'identifiant des représentants
	endRep1 = ''.join(random.choice(string.ascii_lowercase) for i in range(4))
	endRep2 = ''.join(random.choice(string.ascii_lowercase) for i in range(4))

	idRep1 = endRep1.encode()
	idRep2 = endRep2.encode()

	file = open("ramdisk/Rep1", "wb")
	file.write(idRep1)

	file = open("ramdisk/Rep2", "wb")
	file.write(idRep2)

	#Ajout de l'identifiant des représentants à la clé
	keyRep1 = key1+idRep1
	keyRep2 = key2+idRep2

	#Encryptage des clés de responsables
	cryptKeyResp1 = cipherResp1.encrypt(keyResp1)
	cryptKeyResp2 = cipherResp2.encrypt(keyResp2)

	#Encryptage des clés de responsables
	cryptKeyRep1 = cipherRep1.encrypt(keyRep1)
	cryptKeyRep2 = cipherRep2.encrypt(keyRep2)

	# Création d'un fichier contenant la clé correspondante sur chaque clé USB
	file = open("usb1/key1", "wb")
	file.write(cryptKeyResp1)
	file.close()

	file = open("usbBis/key1", "wb")
	file.write(cryptKeyResp1)
	file.close()

	file = open("usb2/key2", "wb")
	file.write(cryptKeyResp2)
	file.close()

	file = open("usbBis/key2", "wb")
	file.write(cryptKeyResp2)
	file.close()

	file = open("usbRep1/key1","wb")
	file.write(cryptKeyRep1)
	file.close()

	file = open("usbBis/key1b", "wb")
	file.write(cryptKeyRep1)
	file.close()

	file = open("usbRep2/key2","wb")
	file.write(cryptKeyRep2)
	file.close()

	file = open("usbBis/key2b", "wb")
	file.write(cryptKeyRep2)
	file.close()

	#Création de la clé maître
	mainKey = get_random_bytes(32)

	#Encryptage de la clé maître
	cipherKey1 = AES.new(key1+("0000".encode()))
	key = cipherKey1.encrypt(mainKey)
	cipherKey2 = AES.new(key2+("0000".encode()))
	mainKey = cipherKey2.encrypt(key)

	#Sauvegarde de la clé maître encryptée sur le ramdisk
	file = open("ramdisk/mainKey", "wb")
	file.write(mainKey)
	file.close()

	# Création des clés de gestion de droit:
	
	keyForResp1 = cipherRep1.encrypt(passResp1)
	keyForResp1 = cipherRep2.encrypt(keyForResp1)
	keyForResp1 = cipherResp2.encrypt(keyForResp1)

	file=open("ramdisk/resp1Key","wb")
	file.write(keyForResp1)
	file.close()

	keyForResp2 = cipherRep2.encrypt(passResp2)
	keyForResp2 = cipherRep1.encrypt(keyForResp2)
	keyForResp2 = cipherResp1.encrypt(keyForResp2)

	file=open("ramdisk/resp2Key","wb")
	file.write(keyForResp2)
	file.close()

	keyForRep1 = cipherResp1.encrypt(passRep1)
	keyForRep1 = cipherRep2.encrypt(keyForRep1)
	keyForRep1 = cipherResp2.encrypt(keyForRep1)

	file=open("ramdisk/rep1Key","wb")
	file.write(keyForRep1)
	file.close()

	keyForRep2 = cipherResp2.encrypt(passRep2)
	keyForRep2 = cipherRep1.encrypt(keyForRep2)
	keyForRep2 = cipherResp1.encrypt(keyForRep2)

	file=open("ramdisk/rep2Key","wb")
	file.write(keyForRep2)
	file.close()

	#encrypt("dd/cards.txt",mainKey)

def repudiate(Role):

	if Role=="Responsable1":
		print("Mot de passe du représentant1:")
		rep1=input()
		print("Mot de passe du représentant2:")
		rep2=input()
		print("Mot de passe du responsable2:")
		resp2=input()
		cipherRep1=AES.new(rep1.encode())
		cipherRep2=AES.new(rep2.encode())
		cipherResp2=AES.new(resp2.encode())
		file=open("ramdisk/resp1Key","rb")
		resp1Crypted=file.readline()
		file.close()
		resp1=cipherResp2.decrypt(resp1Crypted)
		resp1=cipherRep2.decrypt(resp1)
		resp1=cipherRep1.decrypt(resp1)

		file = open("usbBis/key1","rb")
		keyResp1Crypted=file.readline()
		file.close()
		potentialCipherResp1=AES.new(resp1)
		keyPotentialResp1=potentialCipherResp1.decrypt(keyResp1Crypted)
		idToTest=keyPotentialResp1[-4:].decode()

		file=open("ramdisk/Resp1","r")
		idResp1=file.readline()
		file.close()
		if(idToTest==idResp1):
			print("Suppression")
		else:
			print("Erreur")


	if Role=="Responsable2":
		print("Mot de passe du représentant2:")
		rep2=input()
		print("Mot de passe du représentant1:")
		rep1=input()
		print("Mot de passe du responsable1:")
		resp1=input()
		cipherRep2=AES.new(rep2.encode())
		cipherRep1=AES.new(rep1.encode())
		cipherResp1=AES.new(resp1.encode())
		file=open("ramdisk/resp2Key","rb")
		resp2Crypted=file.readline()
		file.close()
		resp2=cipherResp1.decrypt(resp2Crypted)
		resp2=cipherRep1.decrypt(resp2)
		resp2=cipherRep2.decrypt(resp2)

		file = open("usbBis/key2","rb")
		keyResp2Crypted=file.readline()
		file.close()
		potentialCipherResp2=AES.new(resp2)
		keyPotentialResp2=potentialCipherResp2.decrypt(keyResp2Crypted)
		idToTest=keyPotentialResp2[-4:].decode()

		file=open("ramdisk/Resp2","r")
		idResp2=file.readline()
		file.close()
		if(idToTest==idResp2):
			print("Suppression")
		else:
			print("Erreur")

	if Role=="Représentant1":
		print("Mot de passe du responsable1:")
		resp1=input()
		print("Mot de passe du représentant2:")
		rep2=input()
		print("Mot de passe du responsable2:")
		resp2=input()
		cipherResp1=AES.new(resp1.encode())
		cipherRep2=AES.new(rep2.encode())
		cipherResp2=AES.new(resp2.encode())
		file=open("ramdisk/rep1Key","rb")
		rep1Crypted=file.readline()
		file.close()
		rep1=cipherResp2.decrypt(rep1Crypted)
		rep1=cipherRep2.decrypt(rep1)
		rep1=cipherResp1.decrypt(rep1)

		file = open("usbBis/key1b","rb")
		keyRep1Crypted=file.readline()
		file.close()
		potentialCipherRep1=AES.new(rep1)
		keyPotentialRep1=potentialCipherRep1.decrypt(keyRep1Crypted)
		idToTest=keyPotentialRep1[-4:].decode()

		file=open("ramdisk/Rep1","r")
		idRep1=file.readline()
		file.close()
		if(idToTest==idRep1):
			print("Suppression")
		else:
			print("Erreur")

	if Role=="Représentant2":
		print("Mot de passe du responsable2:")
		resp2=input()
		print("Mot de passe du représentant1:")
		rep1=input()
		print("Mot de passe du responsable1:")
		resp1=input()
		cipherResp2=AES.new(resp2.encode())
		cipherRep1=AES.new(rep1.encode())
		cipherResp1=AES.new(resp1.encode())
		file=open("ramdisk/rep2Key","rb")
		rep2Crypted=file.readline()
		file.close()
		rep2=cipherResp1.decrypt(rep2Crypted)
		rep2=cipherRep1.decrypt(rep2)
		rep2=cipherResp2.decrypt(rep2)

		file = open("usbBis/key2b","rb")
		keyRep2Crypted=file.readline()
		file.close()
		potentialCipherRep2=AES.new(rep2)
		keyPotentialRep2=potentialCipherRep2.decrypt(keyRep2Crypted)
		idToTest=keyPotentialRep2[-4:].decode()

		file=open("ramdisk/Rep2","r")
		idRep2=file.readline()
		file.close()
		if(idToTest==idRep2):
			print("Suppression")
		else:
			print("Erreur")


def encrypt(file, key):

	fileToEncrypt = open(file, "r")
	fileEncrypted = open("tmp.h", "wb")

	cipher = AES.new(key)
	l=""
	for line in fileToEncrypt:
		fault=len(line)%16
		if(fault!=0):
			if(len(line)<16):
				line=line+l.join(random.choice(string.ascii_lowercase) for i in range (16-len(line)))
				print(len(line))
			else:
				print(fault)
				r=(16-fault)
				line=line+l.join(random.choice(string.ascii_lowercase) for i in range (r))
			fileEncrypted.write(cipher.encrypt(line.encode()))

	fileToEncrypt.close()
	fileEncrypted.close()




def decrypt(file, key):

	fileDecrypted = open("tmp", "w")
	fileToDecrypt = open(file, "rb")

	fileDecrypted.close()
	fileToDecrypt.close()







def main(args):
	
	init()
	'''start()'''
	'''addCard("565656556565","Dupont")
	addCard("656565656565","Dupont")
	addCard("598762","Diane")
	addCard("59876259","Diane")
	findCard("Diane")
	delCard("656565656565","Dupont")'''

if __name__ == "__main__":
	main(sys.argv)