# TP3: Un fichier de cartes bancaires 

## Binôme 

Nom, Prénom, email: Clopier-Duquenne, Mélanie, melanie.clopierduquenne.etu@univ-lille.fr

Nom, Prénom, email: Dewaele, Audrey, audrey.dewaele.etu@univ-lille.fr

## Question 1

Les 2 responsables ont chacun une clé USB contenant une clé cryptée, et connaissent la clé de décryptage de cette clé que l'on assimile à un mot de passe. L'association de la clé USB ("je possède") et du mot de passe ("je connais") constitue une authentification à 2 facteurs. 
Une fois ces 2 clés décryptées, seule l'association de ces 2 clés dans un ordre connu des responsables seuls permet de décrypter à son tour la clé de maître cryptée contenue sur le disque dur du serveur. Cette clé de maître décryptée, stockée sur la RAM, permet de décrypter toutes les paires nom/n° de carte présents cryptés sur le disque dur du serveur. 

![Schéma question 1](Q1.jpg)

## Question 3

Pour qu'un représentant puisse se substituer au responsable tout en étant identifiable, nous pouvons lui donner une clé USB sur laquelle se trouvera une clé cryptée, et un mot de passe permettant de décrypter la clé, nous restons donc sur le principe de la double authentification avec " ce que je sais" et "ce que j'ai". Pour différencier un responsable de son représentant, nous modifierons légérement la clé afin de produire deux clés cryptées différentes, pour cela, nous ajouterons quelques caractères en fin de clé permettant d'identifier si c'est un responsable ou un représentant.

## Question 5

Le droit de chaque personne responsable sera protégé par une clé cryptée qui ne pourra être décryptée que par les clés des 3 autres dans un ordre précis: 
* La clé du Responsable1 sera cryptée par Représentant1, Représentant2 et Responsable2 et donc décryptable dans l'ordre Responsable2, Représentant2 puis Représentant1
* La clé du Responsable2 sera cryptée par Représentant2, Représentant1et Responsable1 et donc décryptable dans l'ordre Responsable1, Représentant1 puis Représentant2
* La clé du Représentant1 sera cryptée par Responsable1, Représentant2 et Responsable2 et donc décryptable dans l'ordre Responsable2, Représentant2 puis Responsable1
* La clé du Représentant2 sera cryptée par Responsable2, Représentant1 et Responsable1 et donc décryptable dans l'ordre Responsable1, Représentant1 puis Responsable2

Une fois les 3 personnes responsables authentifiées et la clé décryptée, on modifiera la clé du représentant ou responsable associé et donc leur clé commune afin de s'assurer que même si le répudié conserve sa clé USB, il ne puisse plus avoir accès aux données.